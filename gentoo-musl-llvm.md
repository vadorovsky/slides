---
title: Building the whole Linux system with LLVM
---

LLVM and Linux
---

* The most of Linux distributions ship LLVM, but as a secondary compiler for
  users.
* These distros themselves usually don't use LLVM to compile the software they
  provide. GCC still dominates there.
* But does it have to be the case?

<!-- end_slide -->

The answer is: no
---

I'd just like to interject for a moment and show you **LLVM/Linux**, or as
I've recently taken to calling it, **LLVM plus Linux**!

Or, ackchyually, **musl/LLVM/Linux**!

<!-- end_slide -->

How?
---

* Gentoo makes it possible!
* It comes with musl/llvm profile and stage3 tarballs.
* It's still somehow experimental and requires a bit of effort.
* Not everything builds, I had to provide patches, either in Gentoo or in
  upstream projects.

<!-- end_slide -->

But why?
---

* Proving that LLVM ecosystem is mature enough to build the whole operating
  system, not just single pieces of software.
* Favoring libc which makes it easy to link statically and make portable
  software.
* Easy cross-compilation environments.
  * I find LLVM/clang way of cross compiling quite convenient
  * Managing sysroots with **crossdev** from Gentoo.
* Pure fun and proving "GNU/Linux" copypasta wrong.

<!-- end_slide -->

Why don't you just use musl and LLVM on a distro with GNU toolchain?
---

* Many build system configs and scripts just assume GCC and glibc are there.
  If they are, they are often pulled into the build without your consent.

<!-- end_slide -->

What's not the goal?
---

* It's not an ideological crusade against GNU/FSF/GPL.
  * Mostly technical choice.
* It's not about suckless/cat-v type of minimalism.

<!-- end_slide -->

But is there anything from GNU?
---

There is still some GNU software in my setup. Some of it bothers me and is going
to be eventually replaced. Some of it doesn't bother me.

* Bash
  * Doesn't bother me
  * Neverthless, using nushell as default
* GNU coreutils
  * Bothers me a bit, planning to replace with Rust uutils
* autotools, GNU make
  * Doesn't bother me
* GNU tar
  * Doesn't bother me
  * I tried using BSD tar, failed miserably - Makefiles in the Linux kernel
    depend on GNU tar options.
* GPG
  * Bothers me a bit, but very hard to replace - so much software depends on it.
  * Sequoia-PGP seems like a nice alternative.

<!-- end_slide -->

stage3
---

* It's a basic tarball which gentoo.org provides.
* The way of using is unpacking it to a partition and chroot.
* It has only essentials like libc, compiler, bash and emerge/portage (Gentoo's package manager)
* Kernel and everything else has to be built in the chroot session with `emerge`.

```shell
tar -xpf stage3-amd64-musl-llvm-latest.tar.xz -C /mnt/gentoo
chroot /mnt/gentoo /bin/bash
# Configure your compiler options in /etc/portage/make.conf
# Configure dracut and modules.load.d
emerge -av sys-kernel/gentoo-kernel
# Configure a bootloader or make EFI entry for your kernel
emerge -av kde-plasma/plasma-meta
# Reboot and use
```

<!-- end_slide -->

crossdev
---

* Tool from Gentoo for cross building.
* Builds independent sysroots (e.g. `/usr/aarch64-gentoo-linux-musl`).
* You can install any Gentoo packages with `aarch64-gentoo-linux-musl-emerge`.
* Libraries end up always in the sysroot (`/usr/aarch64-gentoo-linux-musl/usr/lib`).
* So far never had issues! I love using it for work.

```shell
emerge -av sys-devel/crossdev
eselect repository create crossdev
crossdev --llvm --target aarch64-gentoo-linux-musl

# Build projects with emerge/portage
aarch64-gentoo-linux-musl-emerge -av sys-libs/zlib

# Build by hand
clang --target=aarch64-gentoo-linux-musl --sysroot=/usr/aarch64-gentoo-linux-musl [...]
```

<!-- end_slide -->

What system I managed to build with LLVM?
---

* Linux kernel
* openrc
* coreutils (both GNU and Rust)
* LLVM, Rust, node.js
* Docker, flatpak
* KDE

<!-- end_slide -->

What about proprietary soft linked to glibc?
---

* The easiest option: run in Docker of flatpak.
* It works for:
  * Discord
  * Steam

<!-- end_slide -->

What had to be patched?
---

* The most of time: removing undefined symbols from link scripts.
* Projects linking to libraries with undefined symbols, not linking to the
  actual library.
* Pointing to musl-compatible alteriatives when glibc is not available.
* The only single case (IMO) putting clang in bad light (in btrfs-progs) -
  clang doesn't fail when you provide flags unsupported by the current CPU
  (e.g. `-msse2`). Such checks are sometimes used for figuring out cpuflags.

<!-- end_slide -->

Which projects exactly?
---

* bluez
* btrfs-progs
* libblockdev
* libnftnl
* keyutils
* KDE Dolphin (patch already released)
* helix
* QEMU

<!-- end_slide -->

libblockdev patch
---

```diff
diff --git a/tools/Makefile.am b/tools/Makefile.am
index 06e46d26e..817af8139 100644
--- a/tools/Makefile.am
+++ b/tools/Makefile.am
@@ -4,10 +4,10 @@ bin_PROGRAMS = lvm-cache-stats vfat-resize
 lvm_cache_stats_CFLAGS   = $(GLIB_CFLAGS) $(BYTESIZE_CFLAGS) -Wall -Wextra -Werror
 lvm_cache_stats_CPPFLAGS = -I${builddir}/../include/
 lvm_cache_stats_LDFLAGS  = -Wl,--no-undefined
-lvm_cache_stats_LDADD    = ${builddir}/../src/lib/libblockdev.la $(GLIB_LIBS) $(BYTESIZE_LIBS)
+lvm_cache_stats_LDADD    = ${builddir}/../src/lib/libblockdev.la $(GLIB_LIBS) $(BYTESIZE_LIBS) $(UUID_LIBS)
 
 vfat_resize_CFLAGS   = $(GLIB_CFLAGS) $(BYTESIZE_CFLAGS) $(PARTED_CFLAGS) $(PARTED_FS_CFLAGS) -Wall -Wextra -Werror
 vfat_resize_CPPFLAGS = -I${builddir}/../include/
 vfat_resize_LDFLAGS  = -Wl,--no-undefined
-vfat_resize_LDADD    = ${builddir}/../src/lib/libblockdev.la $(GLIB_LIBS) $(BYTESIZE_LIBS) $(PARTED_LIBS) $(PARTED_FS_LIBS)
+vfat_resize_LDADD    = ${builddir}/../src/lib/libblockdev.la $(GLIB_LIBS) $(BYTESIZE_LIBS) $(PARTED_LIBS) $(PARTED_FS_LIBS) $(UUID_LIBS)
 endif
```

<!-- end_slide -->

libnftnl patch
---

(not mine)

```diff
diff --git a/include/libnftnl/chain.h b/include/libnftnl/chain.h
index f56e581..bac1f5f 100644
--- a/include/libnftnl/chain.h
+++ b/include/libnftnl/chain.h
@@ -71,10 +71,6 @@ struct nlmsghdr;
 
 void nftnl_chain_nlmsg_build_payload(struct nlmsghdr *nlh, const struct nftnl_chain *t);
 
-int nftnl_chain_parse(struct nftnl_chain *c, enum nftnl_parse_type type,
-		    const char *data, struct nftnl_parse_err *err);
-int nftnl_chain_parse_file(struct nftnl_chain *c, enum nftnl_parse_type type,
-			 FILE *fp, struct nftnl_parse_err *err);
 int nftnl_chain_snprintf(char *buf, size_t size, const struct nftnl_chain *t, uint32_t type, uint32_t flags);
 int nftnl_chain_fprintf(FILE *fp, const struct nftnl_chain *c, uint32_t type, uint32_t flags);
 
diff --git a/src/libnftnl.map b/src/libnftnl.map
index ad8f2af..8fffff1 100644
--- a/src/libnftnl.map
+++ b/src/libnftnl.map
@@ -47,8 +47,6 @@ global:
   nftnl_chain_get_s32;
   nftnl_chain_get_u64;
   nftnl_chain_get_str;
-  nftnl_chain_parse;
-  nftnl_chain_parse_file;
   nftnl_chain_snprintf;
   nftnl_chain_fprintf;
   nftnl_chain_nlmsg_build_payload;
@@ -174,8 +172,6 @@ global:
   nftnl_set_elems_nlmsg_build_payload;
   nftnl_set_elems_nlmsg_parse;
 
-  nftnl_set_elems_foreach;
-
   nftnl_set_elems_iter_create;
   nftnl_set_elems_iter_cur;
   nftnl_set_elems_iter_next;
```

<!-- end_slide -->

QEMU patch
---

```diff
diff --git a/linux-user/syscall.c b/linux-user/syscall.c
index bbba2a6..38fa09a 100644
--- a/linux-user/syscall.c
+++ b/linux-user/syscall.c
@@ -6812,13 +6812,13 @@ static int target_to_host_fcntl_cmd(int cmd)
         ret = cmd;
         break;
     case TARGET_F_GETLK:
-        ret = F_GETLK64;
+        ret = F_GETLK;
         break;
     case TARGET_F_SETLK:
-        ret = F_SETLK64;
+        ret = F_SETLK;
         break;
     case TARGET_F_SETLKW:
-        ret = F_SETLKW64;
+        ret = F_SETLKW;
         break;
     case TARGET_F_GETOWN:
         ret = F_GETOWN;
```

I still need to make an upstreamable version of that.

<!-- end_slide -->

btrfs-progs patch
---

```diff
diff --git a/configure.ac b/configure.ac
index 1850048bb..ddeab2349 100644
--- a/configure.ac
+++ b/configure.ac
@@ -45,19 +45,73 @@ AC_C_CONST
 AC_C_VOLATILE
 AC_C_BIGENDIAN
 
-AX_CHECK_COMPILE_FLAG([-msse2], [HAVE_CFLAG_msse2=1], [HAVE_CFLAG_msse2=0])
+AC_MSG_CHECKING([whether the compiler supports -msse2])
+AC_COMPILE_IFELSE([AC_LANG_SOURCE([[
+#include <emmintrin.h>
+
+int main() {
+    __m128i a = _mm_setzero_si128();
+    return 0;
+}
+]])],
+    [AC_MSG_RESULT([yes])
+     HAVE_CFLAG_msse2=1],
+    [AC_MSG_RESULT([no])
+     HAVE_CFLAG_msse2=0])
 AC_SUBST([HAVE_CFLAG_msse2])
 AC_DEFINE_UNQUOTED([HAVE_CFLAG_msse2], [$HAVE_CFLAG_msse2], [Compiler supports -msse2])
```

<!-- end_slide -->

llvm-libgcc
---

* On the most distros, libgcc_s is the system unwinder.
* llvm-libgcc (libunwind + compiler-rt + GNU-compatible symbols) works just
  fine as a replacement.
* So far no official Gentoo package, available in third-party overlays.
  * Kudos to https://github.com/12101111 for initial packaging.
  * My overlay has a forked package, more cross-build friendly.

<!-- end_slide -->

Conclusion
---

* LLVM is definitely mature enough to build the whole system.
* The biggest obstacle is GNU toolchain being forgiving with doing silly
  mistakes and LLVM+Gentoo users having to fix them.
* Another obstacle is usage of glibc extensions.
* For proprietary software (Discord, Steam) musl would be a great choice, but
  they still use glibc.

<!-- end_slide -->

Trying to make things easier
---

* https://gitlab.com/vadorovsky/gentoo-musl-llvm - stage4, ready-to-use
  Gentoo with musl+LLVM without having to actually "install Gentoo". 😛
* https://gitlab.com/vadorovsky/overlay - my Gentoo package repo with patches.
* Using Docker to build the whole system image. All the burden of compiling the
  system is done by me! Resulting images are in the registry.

<!-- end_slide -->

Container images
---

* `docker pull https://gitlab.com/vadorovsky/gentoo-musl-llvm:stage4-amd64-toolchains`
  * Everything you need for native builds with LLVM, rustc and node.js.
  * Works on Github Actions and Gitlab CI.
* `docker pull https://gitlab.com/vadorovsky/gentoo-musl-llvm:stage4-amd64-toolchains-cross`
  * Same, but crossdev available for cross builds.

<!-- end_slide -->

Desktop installation
---

* `install.sh` script
  * https://gitlab.com/vadorovsky/gentoo-musl-llvm/-/blob/main/install.sh
  * It wipes the provided disks.
  * Sets up BTRFS (RAID0).
  * Unpacks the `stage4-openrc-kde` variant of Docker image to that new
    filesystem.
  * Copies kernel and initramfs to EFI partition.
  * Creates EFI entry.
* KDE, openrc.

<!-- end_slide -->

Misc differences of my stage4 from traditional distros
---

* No GRUB or other traditional bootloader. Kernel booted directly with EFI.
* Bias towards Rust-based tools like:
  * nushell as default shell
  * helix as default editor

<!-- end_slide -->

Other projects
---

* **Chimera Linux** - a full binary distro using musl+LLVM and BSD userland as
  coreutils replacement.
* **iglinux** - an attempt to make a musl+LLVM distro. No ISOs or way to
  install without building all packages yourself. Limited amount of software.

<!-- end_slide -->

Future plans
---

* Make it work on Macbook with Asahi kernel.
  * u-boot (specifically - Asahi's patches 😅) doesn't link with LLD.
  * Creating EFI entries on Mac is not as nice as with usual x86_64 machines,
    bootloader is actually needed here for managing different kernels.
* Make zig work with musl (or in general - without glibc).
* Make node.js alternatives work:
  * Deno doesn't build on Rust musl targets.
  * zig doesn't build without glibc, therefore Bun also doesn't.
* Maybe evolve the project in direction of Gentoo remix/derivative. Especially
  if more people are interested.
* Rust uutils as full coreutils replacement.

<!-- end_slide -->

What about LLVM libc?
---

* Gentoo doesn't support LLVM libc yet.
* I'd be happy to help there once I get more time.
* I'd be happy to replace musl with LLVM libc in my stage4 if ever possible.
* It would make the project really unique.

<!-- end_slide -->

Questions?
---

<!-- end_slide -->
