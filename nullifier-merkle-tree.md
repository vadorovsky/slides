---
title: Nullifier Merkle tree
---

Merkle tree
---

* Balanced binary tree.
* Nodes in bottom rows are hashes of inserted data.
* Each parent node is a hash of their children.

```
      H(H(A, B), H(C,D))
      /               \
   H(A, B)         H(C, D)
  /       \       /       \
H(A)     H(B)   H(C)     H(D)
  |       |       |       |
-----------------------------
  |       |       |       |
  A       B       C       D
```

<!-- end_slide -->

Sparse Merkle tree
---

<!-- end_slide -->

Different Merkle trees
---

| Transaction MT       | Event MT                 | Nullifier MT                  |
| -------------------- | ------------------------ | ----------------------------- |
| Commitments to UTXOs | Notifications for indxer | Indicators that UTXO was used |
| Stores UTXOs         | Stores events            | Stores nullifiers             |
| Append only          | Append only              | Insert                        |
| Sparse MT            | Sparse MT                | Sparse MT + indexing array    |

<!-- end_slide -->

Resources about nullifier MT
---

* [Aztec's documentation](https://docs.aztec.network/concepts/advanced/data_structures/indexed_merkle_tree).
* Read also [Aztec's code](https://github.com/AztecProtocol/barretenberg/blob/master/cpp/src/barretenberg/stdlib/merkle_tree/nullifier_tree/nullifier_tree.hpp).

<!-- end_slide -->

Sparse MT for nullifiers - what's the problem?
---

* Nullifiers can be of any value of the elliptic curve's prime field.
* For bn254, it means a tree of depth 254.

<!-- end_slide -->

Indexed Merkle tree
---

* We have a sparse (thus append only) Merkle tree:
  * Each leaf is extended by information about leaf with the next higher value.
    * `H(value, next_index, next_value)`
  * This way, the tree keeps a linked list property.
  * That linked is used to prove that a nullifier is not in a list.
* We have an array consisting of unhashed values and next indexes for range
  lookup. It's mutable.
  * It's fine to store nullifiers as plain text.

| Value      | Value      | ... |
| Next index | Next index | ... |

<!-- end_slide -->

Oversimplified example
---

We want to spend nullifier 7. We can do it, because range 5-10 doesn't exist in
the tree.

```
1 -> 5 -> 10
```

After spending the nullifier:

```
1 -> 5 -> 7 -> 10
```

<!-- end_slide -->

Low nullifier
---

For every new nullifier we want to use, there is a low nullifier where:

* The next value is greater than the new one:
  * `low_nullifier.next_value > new_value`
* If no such nullifier is found, that means our nullifier is the greatest. In
  such case, the low nullifier is the current greatest nullifier:
  * `low_nullifier.next_value == 0`

<!-- end_slide -->

Extending the tree
---

* It's still a sparse Merkle tree, so it's append only!
  * We append `H(value, next_index, next_value)`, even if we insert a nullifier
    within already existing ranges.
* We use indexing array to figure out `next_index` and  `next_value`.

<!-- end_slide -->

Basic insertion algorithm
---

* In the array:
  * Find the low nullifier.
  * Insert new node.
  * Update the `next_index` field in the low nullifier, to point to the new
    node.
* In the sparse Merkle tree:
  * Yield the updated low nullifier as a new leaf.
  * Yield the new node as a new leaf.

<!-- end_slide -->

Initial state
---

|            |   |   |   |   |   |   |   |   |
| ---------- | - | - | - | - | - | - | - | - |
| Value      | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| Next index | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |

```
              Ø
             / \
       /----/   \----\
      Ø               Ø
    -/ \-           -/ \-
   /     \         /     \
  Ø       Ø       Ø       Ø
 / \     / \     / \     / \
Ø   Ø   Ø   Ø   Ø   Ø   Ø   Ø
```

<!-- end_slide -->

Insert 30
---

|            | Low nullifier         | **New nullifier** |   |   |   |   |   |   |
| ---------- | --------------------- | ----------------- | - | - | - | - | - | - |
| Value      | 0                     | **30**            | 0 | 0 | 0 | 0 | 0 | 0 |
| Next index | **(new nullifier) 1** | 0                 | 0 | 0 | 0 | 0 | 0 | 0 |

```
                                  H
                   /-------------/ \-----------\
                   H                            Ø
           /------/ \------\             /-----/ \-----\
          H                 Ø           Ø               Ø
     ----/ \----           / \         / \             / \
    /           \         /   \       /   \           /   \
H(0,1,30)   H(30,0,0)    Ø     Ø     Ø     Ø         Ø     Ø
```


<!-- end_slide -->

Insert 10
--

|            | Low nullifier         | Next nullifier | **New nullifier**      |   |   |   |   |   |
| ---------- | --------------------- | -------------- | ---------------------- | - | - | - | - | - |
| Value      | 0                     | 30             | **10**                 | 0 | 0 | 0 | 0 | 0 |
| Next index | **(new nullifier) 2** | 0              | **(next nullifier) 1** | 0 | 0 | 0 | 0 | 0 |

```
                                           H
                       /------------------/ \------------------\
                      H                                         Ø
           /---------/ \---------\                        /----/ \----\
          H                       H                      Ø             Ø
     ----/ \----             ----/ \----                / \           / \
    /           \           /           \              /   \         /   \
H(0,1,30)   H(30,0,0)   H(0,2,10)   H(10,1,30)        Ø     Ø       Ø     Ø
```

<!-- end_slide -->

Insert 20
---

|            |   | Next nullifier | Low nullifier         | **New nullifier**      |   |   |   |   |
| ---------- | - | -------------- | --------------------- | ---------------------- | - | - | - | - |
| Value      | 0 | 30             | 10                    | **20**                 | 0 | 0 | 0 | 0 |
| Next index | 2 | 0              | **(new nullifier) 3** | **(next nullifier) 1** | 0 | 0 | 0 | 0 |

```
                                              H
                       /---------------------/ \---------------------\
                      H                                               H
           /---------/ \---------\                             /-----/ \-----\
          H                       H                           H               Ø
     ----/ \----             ----/ \----                 ----/ \----         / \
    /           \           /           \               /           \       /   \
H(0,1,30)   H(30,0,0)   H(0,2,10)   H(10,1,30)    H(10,3,20)   H(20,1,30)  Ø     Ø
```

<!-- end_slide -->

Insert 50
---

Let's insert 50

|            |   | Low nullifier         |    |    | **New nullifier** |   |   |   |
| ---------- | - | --------------------- | -- | -- | ----------------- | - | - | - |
| Value      | 0 | 30                    | 10 | 20 | **50**            | 0 | 0 | 0 |
| Next index | 2 | **(new nullifier) 4** | 3  | 1  | 0                 | 0 | 0 | 0 |

```
                                                H
                       /-----------------------/ \-----------------------\
                      H                                                   H
           /---------/ \---------\                             /---------/ \---------\
          H                       H                           H                       H
     ----/ \----              ----/ \----                ----/ \----             ----/ \----
    /           \            /           \              /           \           /           \
H(0,1,30)   H(30,0,0)   H(0,2,10)   H(10,1,30)    H(10,3,20)   H(20,1,30)  H(30,4,50)   H(50,0,0)
```

<!-- end_slide -->


Proving non-membership
---

* While proving non-membership, the low nullifier is what matters.
  * That's why it's fine we don't update other leaves in the sparse MT.

* Find the low nullifier and hash it.
* Prove the low nullifier leaf exists in the tree.
* Check that the new nullifier "would have belonged" to the range.
  * If low_nullifier.next_index == 0:
    * That means low nullifier is the highest one. New value must be higher
      than all values in the tree.
    * assert(low_nullifier.value < new_value)
  * Else:
    * assert(low_nullifier.value < new_value)
    * assert(low_nullifier.next_value > new_value)

<!-- end_slide -->

Batched insertion
---

* In previous example, we were yielding leaves by each insertion.
* We already have a separate array with unhashed nullifiers!
  * Why don't we just use it as a queue as long as we fill up the subtree? 🤔
